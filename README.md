# Telegram Send

This is a Telegram bot written in go that is aimed to be used in crontabs.
Therefore it reads the message to send from stdin.
It has both a message rate limit and an error rate limit (in minutes) in order to
limit the amount of Telegram messages and emails recieved in case of errors.
(In case your cronjob are executed very frequently.)
Currently there is no message duplication check.

## Building
It is created as a shell script, and will be compiled upon execution and cached
for later use. So no bulid step.

## How to run
First time:

`echo "hello" | ./telegram-send.go -token <your telegram bot token>`

Then you can skip the `token` option.






