package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"
	"bytes"
	"mime/multipart"
)

// From: https://mholt.github.io/json-to-go/
type TelegramUpdates struct {
	Ok     bool `json:"ok"`
	Result []struct {
		UpdateID int `json:"update_id"`
		Message  struct {
			MessageID int `json:"message_id"`
			From      struct {
				ID           int    `json:"id"`
				IsBot        bool   `json:"is_bot"`
				FirstName    string `json:"first_name"`
				LastName     string `json:"last_name"`
				LanguageCode string `json:"language_code"`
			} `json:"from"`
			Chat struct {
				ID        int    `json:"id"`
				FirstName string `json:"first_name"`
				LastName  string `json:"last_name"`
				Type      string `json:"type"`
			} `json:"chat"`
			Date int    `json:"date"`
			Text string `json:"text"`
		} `json:"message"`
	} `json:"result"`
}

func cmd(url string, target interface{}) error {
	telegram := http.Client{
		Timeout: time.Second * 5,
	}

	if req, err := http.NewRequest(http.MethodGet, url, nil); err == nil {
		if resp, err := telegram.Do(req); err == nil {
			defer resp.Body.Close()
			return json.NewDecoder(resp.Body).Decode(target)
		} else {
			return err
		}
	} else {
		return err
	}
}

func (settings *Settings) getUpdates() (*TelegramUpdates, error) {
	var updates TelegramUpdates
	err := cmd(fmt.Sprintf("https://api.telegram.org/bot%s/getUpdates", settings.Token), &updates)

	return &updates, err
}

func (settings *Settings) sendMessage(msg string) error {
	url := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage?text=%s&chat_id=%d",
		settings.Token, url.PathEscape(msg), settings.ChatID)
	return cmd(url, new(interface{}))
}

func (settings *Settings) sendImage(fileName, caption string) error {

	file, err := os.Open(fileName)

	if err != nil {
		return err
	}
	defer file.Close()

	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)

	fw, err := w.CreateFormFile("photo", fileName)

	if  err != nil {
                return err
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		return err
	}

	w.Close()

	url := fmt.Sprintf("https://api.telegram.org/bot%s/sendPhoto?caption=%s&chat_id=%d",
		settings.Token, url.PathEscape(caption), settings.ChatID)

	req, err := http.NewRequest("POST", url, buf)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())

	telegram := http.Client{
		Timeout: time.Second * 5,
	}

	resp, err := telegram.Do(req)
	defer resp.Body.Close()

	return err
}

type Settings struct {
	ChatID           int       `json:"chat_id"`
	Token            string    `json:"token"`
	ErrorTime        time.Time `json:"time"`
	MessageTime      time.Time `json:"time"`
	ErrorRateLimit   int       `json:"error_rate_limit"`
	MessageRateLimit int       `json:"message_rate_limit"`
}

const PATH = "${HOME}/.config/telegram-send/"
const SETTINGSFILE = "settings.json"
const DEFAULT_ERROR_RATE_LIMIT = 60   /* Minutes */
const DEFAULT_MESSAGE_RATE_LIMIT = 10 /* Minutes */

func (settings *Settings) load() error {
	settings.ErrorRateLimit = DEFAULT_ERROR_RATE_LIMIT
	settings.MessageRateLimit = DEFAULT_MESSAGE_RATE_LIMIT

	if f, err := os.Open(filepath.Join(os.ExpandEnv(PATH), SETTINGSFILE)); err == nil {
		defer f.Close()
		err = json.NewDecoder(f).Decode(settings)
		return err
	} else {
		/* Failing to load settings at first time isn't really an error */
		return nil
	}
}

func (settings *Settings) save() error {
	err := os.MkdirAll(os.ExpandEnv(PATH), os.ModePerm)
	if err != nil {
		return err
	}

	if f, err := os.Create(filepath.Join(os.ExpandEnv(PATH), SETTINGSFILE)); err == nil {
		defer f.Close()
		if settingsJson, err := json.Marshal(*settings); err == nil {
			f.Write(settingsJson)
			return nil
		} else {
			return err
		}
	} else {
		return err
	}
}

func (settings *Settings) errorMsg(err error) {
	delta := int(time.Now().Sub(settings.ErrorTime) / time.Minute)

	if delta > settings.ErrorRateLimit {
		settings.ErrorTime = time.Now()
		settings.save() /* Ignore eventual returned errors */
	}
	os.Exit(0)
}

func main() {
	/*
	 * Go's flag package can't help you if a command line option was set via
	 * command line or not, therefore the weird default value.
	 */

	token := flag.String("token", "", "telegram bot token")
	error_rate_limit := flag.Int("erl", -1, "Error rate limit in minutes")
	message_rate_limit := flag.Int("mrl", -1, "Message rate limit in minutes")

	image := flag.String("image", "", "Image to send")
	caption := flag.String("caption", "", "Image caption")

	flag.Parse()

	var settings Settings

	if err := settings.load(); err != nil {
		settings.errorMsg(err)
	}

	if len(*token) > 0 {
		settings.Token = *token
	}

	if *error_rate_limit >= 0 {
		settings.ErrorRateLimit = *error_rate_limit
	}

	if *message_rate_limit >= 0 {
		settings.MessageRateLimit = *message_rate_limit
	}

	if len(settings.Token) == 0 {
		settings.errorMsg(fmt.Errorf("ERROR: No token available."))
	}

	if settings.ChatID == 0 {
		updates, err := settings.getUpdates()
		if err != nil {
			settings.errorMsg(err)
		}

		l := len(updates.Result)

		if l == 0 {
			settings.errorMsg(fmt.Errorf("ERROR: No resent chats to fetch chat id from. Send a message.\n"))
		}
		settings.ChatID = updates.Result[l-1].Message.Chat.ID
	}

	delta := int(time.Now().Sub(settings.MessageTime) / time.Minute)

	if delta > settings.MessageRateLimit {

		if len(*image) == 0 {

			raw_msg, err := ioutil.ReadAll(os.Stdin)
			if err != nil {
				settings.errorMsg(err)
			}

			err = settings.sendMessage(string(raw_msg))
			if err != nil {
				settings.errorMsg(err)
			}
		} else {
			settings.sendImage(*image, *caption)
		}
		settings.MessageTime = time.Now()

	}

	if err := settings.save(); err != nil {
		settings.errorMsg(err)
	}
}
